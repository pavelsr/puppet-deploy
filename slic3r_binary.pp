include wget

$fbhomefolder = '/home/fablab'
$curr_release = 'slic3r-linux-x86_64-1-2-9-stable.tar.gz'

wget::fetch { "download latest binary version of Slic3r for Linux (x64)":
  source      => 'http://dl.slic3r.org/linux/${curr_release}',
  destination => '${fbhomefolder}/Reprap/Slic3r',
  timeout     => 0,
  verbose     => true,
}

exec{'Unzip Slic3r':
path => '${fbhomefolder}/Reprap/Slic3r',
command => "tar -zxvf ${curr_release}",
user => 'root',
}

