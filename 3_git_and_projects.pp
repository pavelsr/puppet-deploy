# Do a local git configuration and download ssh keys

# include https://forge.puppetlabs.com/puppetlabs/apt module
# sudo puppet module install puppetlabs-apt


package{'git':
ensure => latest,
}

git::config { 'user.name':
  value => 'Pavel Serikov',
}

git::config { 'user.email':
  value => 'pavel.p.serikov@gmail.com',
}
