include apt

apt::ppa { 'ppa:inkscape.dev/stable': }

package{'inkscape':
ensure => latest,
}
