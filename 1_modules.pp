# install all necessary modules from puppet forge
# replace bad commands like 'sudo puppet module install puppetlabs-apt'
# for this manifest work you need to install rcoleman-puppet_module:
# sudo puppet module install rcoleman-puppet_module
# (https://forge.puppetlabs.com/rcoleman/puppet_module)

module { 'puppetlabs/git':
  ensure   => present,
}

module { 'puppetlabs/apt':
  ensure   => present,
}

module { 'puppetlabs/vcsrepo':
  ensure   => present,
}

module { 'maestrodev/wget':
  ensure   => present,
}
