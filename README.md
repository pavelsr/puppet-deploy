# Configuration for easy setup Ubuntu system

run it as 

```
puppet apply 1_modules.pp
```

etc.

# Pre-setup

```
sudo apt-get update
sudo apt-get upgrade
ssh-keygen
apt-get install git
# + add ssh key to your bibucket account
git clone git@bitbucket.org:serikov/puppet-deploy.git

sudo puppet module install puppetlabs-apt
sudo puppet module install rcoleman-puppet_module

```
