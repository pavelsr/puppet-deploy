include apt
include git

package{'git':
ensure => latest,
}

package{'synaptic':
ensure => latest,
}

package{'aptitude':
ensure => latest,
}

package{'sshpass':
ensure => latest,
}

apt::ppa { 'ppa:inkscape.dev/stable': }

package{'inkscape':
ensure => latest,
}

package{'vlc':
ensure => latest,
}

