$fbhomefolder = '/home/fablab'

file { $fbhomefolder :
    ensure => "directory",
}

user{'fablab_user':
name => 'fablab',
password => '2014',
}

vcsrepo { "${fbhomefolder}/Lasersaur/LasaurApp":
  ensure   => present,
  provider => git,
  source   => 'git://github.com/stefanix/LasaurApp.git',
}

vcsrepo { "${fbhomefolder}/Reprap/Pronterface":
  ensure   => present,
  provider => git,
  source   => 'git://github.com/reprappro/Software.git',
}

vcsrepo { "${fbhomefolder}/Reprap/Pronterface":
  ensure   => present,
  provider => git,
  source   => 'git://github.com/reprappro/Software.git',
}
